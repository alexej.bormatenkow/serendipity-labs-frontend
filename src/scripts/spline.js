// creates an interpolated line between given points and returns array of 2d points
// basic implementation with linear interpolation


// provide tuple of points (x,y) and pressure (z)
// inputPoints = [[x,y,z], [x,y,z], [x,y,z], ...]
// returns array of 2d points


import { linearInterpolation } from './vector.js'

let strokePoints = [];

export function addPoint(x, y) {
    points.push([x, y]);


    console.log("points: " + x + ", " + y);
    console.log("linear interpolation: " + linearInterpolation([x,y], [x,y], 0.5));
}


export function getLine(points, options) {
    const t = 0.15 + (1 - options.smoothness) * 0.85


    if (points.length === 1) {
        return points;
    }


    // insert a point between two points to avoid jagged lines
    if (points.length === 2) {
        let last = points[1];
        points.slice(0, -1);
        for (let i = 0; i < 5; i++) {
            points.push(linearInterpolation(points[0], last, i / 4));
        }
    }

    //console.log("points: " + points);

    let prev = points[0];

    for (let i = 1; i < points.length; i++) {
        let current = points[i];
        // console.log("given: " + prev + ", " + current + ", " + t);
        let interpolated = linearInterpolation(prev, current, t);
        // console.log("interpolated: " + interpolated);
        strokePoints.push(interpolated);
        prev = interpolated;
    }

    return strokePoints;

}

function getSplinePoint(t, points, currentPoint) {

}

