// some useful vector functions

// add
export function add(a, b) {
    return [a[0] + b[0], a[1] + b[1]];
}

// subtract
export function subtract(a, b) {
    return [a[0] - b[0], a[1] - b[1]];
}

// multiply with scalar
export function multiply(a, s) {
    return [a[0] * s, a[1] * s];
}

// divide by scalar
export function divide(a, s) {
    return [a[0] / s, a[1] / s];
}

// multiply two vectors
export function multiplyVectors(a, b) {
    return [a[0] * b[0], a[1] * b[1]];
}

// distance
export function distance(a, b) {
    return Math.sqrt(Math.pow(a[0] - b[0], 2) + Math.pow(a[1] - b[1], 2));
}

// dot
export function dot(a, b) {
    return a[0] * b[0] + a[1] * b[1];
}

// cross
export function cross(a, b) {
    return a[0] * b[1] - a[1] * b[0];
}


// linear interpolation
export function linearInterpolation(a, b, t) {
    return add(a, multiply(subtract(b, a), t));
}
