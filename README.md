# serendipity-labs-frontend

A complete rewrite of the serendipity-labs frontend in Vue 3 and SASS

Some functionality depends on the go-rest backend

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
![serendipity-labs-frontend-desktop](landing-desktop.png)
